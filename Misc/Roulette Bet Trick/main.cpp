#include <iostream>
#include <string>

int main ()
{
	int bet = 0;
	int max = 0;

	std::cout << "What is your initial bet?\n";
	std::cin >> bet;
	std::cout << "How many times will you double your bet if you lose the previous?\n";
	std::cin >> max;
	for(int i = 0; i < max; i++)
	{
		std::cout << std::endl << std::endl;
		std::cout << i+1 << "\t$" << bet << "\n";
		bet *= 2;
	}

	std::cin.ignore ();
	std::cin.get ();
}