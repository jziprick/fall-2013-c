#include <iostream>                     // use input/output library.

using namespace std;                    // use standard location.

int main(){                             // start main function.
    string greeting = "Hello world.";   // assign "Hello world." to variable string greeting.
    cout << greeting << endl;           // output variable string greeting.
}                                       // end main function.