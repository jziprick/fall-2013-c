#include <iostream>
#include <string>

/*
Create a function that takes
two strings and appends the
vowels of the second string
to the first string. Create
a driver to test your code.
Account for errors.
*/


void append (std::string& str1, std::string& str2)
{
	str1 += str2;
}

void parseVowels (std::string& str2)
{
	std::string vowels;
	int length = str2.length();
	for(int i = 0; i < length; i++)
	{
		switch(str2.at(i))
		{
			case 'a': 
			case 'e': 
			case 'i': 
			case 'o': 
			case 'u': 
			case 'y':
				vowels += str2.at(i);
		}
	}
	str2 = vowels;
}

int main ()
{
	std::string str1;
	std::string str2;

	std::cout << "Enter first string: ";
	std::cin >> str1;
	std::cout << "Enter second string: ";
	std::cin >> str2;
	parseVowels(str2);
	append(str1, str2);
	std::cout << "Result:\n"
		<< str1;
	std::cin.ignore();
	std::cin.get();
}