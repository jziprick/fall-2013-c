// Jonathan Ziprick
// ID 2505811
// jziprick1@student.rcc.edu
// HW2

// 1)

// a)

#include iostream

// The angle brackets are missing around
// "iostream". The syntax (arrangement of
// the language) in C++ requires angle
// brackets around the inclusion of a
// library file. Therefore, this omission
// will cause a compiler error.

// b)

int x = 2 / 0;

// Although the syntax is correct, the
// initialization of the variable integer
// "x" is 2 divided by 0 which result in
// a runtime error, because 2 is not
// divisible by 0.