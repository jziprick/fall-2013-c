// Jonathan Ziprick
// ID 2505811
// jziprick1@student.rcc.edu
// HW2

// 3)

#include <iostream>
#include <iomanip>

using namespace std;

int main ()
{

	// Variables
	double john_quiz1 = 0;
	double john_quiz2 = 0;
	double john_quiz3 = 0;
	double john_quiz4 = 0;
	double mary_quiz1 = 0;
	double mary_quiz2 = 0;
	double mary_quiz3 = 0;
	double mary_quiz4 = 0;
	double matthew_quiz1 = 0;
	double matthew_quiz2 = 0;
	double matthew_quiz3 = 0;
	double matthew_quiz4 = 0;
	double avg_quiz1 = 0;
	double avg_quiz2 = 0;
	double avg_quiz3 = 0;
	double avg_quiz4 = 0;

	// Input of John's Quizzes
	cout << "Enter the Quiz 1 score for John: ";
	cin >> john_quiz1;
	cout << endl << "Enter the Quiz 2 score for John: ";
	cin >> john_quiz2;
	cout << endl << "Enter the Quiz 3 score for John: ";
	cin >> john_quiz3;
	cout << endl << "Enter the Quiz 4 score for John: ";
	cin >> john_quiz4;

	// Input of Mary's Quizzes
	cout << endl << endl << endl << "Enter the Quiz 1 score for Mary: ";
	cin >> mary_quiz1;
	cout << endl << "Enter the Quiz 2 score for Mary: ";
	cin >> mary_quiz2;
	cout << endl << "Enter the Quiz 3 score for Mary: ";
	cin >> mary_quiz3;
	cout << endl << "Enter the Quiz 4 score for Mary: ";
	cin >> mary_quiz4;

	// Input of Matthew's Quizzes
	cout << endl << endl << endl << "Enter the Quiz 1 score for Matthew: ";
	cin >> matthew_quiz1;
	cout << endl << "Enter the Quiz 2 score for Matthew: ";
	cin >> matthew_quiz2;
	cout << endl << "Enter the Quiz 3 score for Matthew: ";
	cin >> matthew_quiz3;
	cout << endl << "Enter the Quiz 4 score for Matthew: ";
	cin >> matthew_quiz4;
	cout << endl << endl << endl;

	// Table Calculations
	avg_quiz1 = (john_quiz1 + mary_quiz1 + matthew_quiz1) / 3;
	avg_quiz2 = (john_quiz2 + mary_quiz2 + matthew_quiz2) / 3;
	avg_quiz3 = (john_quiz3 + mary_quiz3 + matthew_quiz3) / 3;
	avg_quiz4 = (john_quiz4 + mary_quiz4 + matthew_quiz4) / 3;

	// BEGIN TABLE
	
	// Row - Column Names
	cout 
		<< "Name" 
		<< setw(10) 
		<< "Quiz 1" 
		<< setw(10) 
		<< "Quiz 2" 
		<< setw(10) 
		<< "Quiz 3" 
		<< setw(10) 
		<< "Quiz 4" 
		<< endl;

	// Row - Dividers
	cout 
		<< "----" 
		<< setw(10) 
		<< "------" 
		<< setw(10) 
		<< "------" 
		<< setw(10) 
		<< "------" 
		<< setw(10)
		<< "------" 
		<< endl;

	// Row - John
	cout 
		<< "John" 
		<< setw(10) 
		<< john_quiz1 
		<< setw(10) 
		<< john_quiz2 
		<< setw(10) 
		<< john_quiz3 
		<< setw(10) 
		<< john_quiz4 
		<< endl;

	// Row - Mary
	cout 
		<< "Mary" 
		<< setw(10) 
		<<  mary_quiz1 
		<< setw(10) 
		<<  mary_quiz2 
		<< setw(10) 
		<<  mary_quiz3 
		<< setw(10) 
		<<  mary_quiz4 
		<< endl;

	// Row - Matthew
	cout 
		<< "Matthew" 
		<< setw(7) 
		<< matthew_quiz1 
		<< setw(10) 
		<< matthew_quiz2 
		<< setw(10) 
		<< matthew_quiz3 
		<< setw(10) 
		<< matthew_quiz4 
		<< endl << endl;

	// Row - Average
	cout 
		<< setprecision(2) 
		<< fixed 
		<< "Average" 
		<< setw(7)
		<< avg_quiz1 
		<< setw(10) 
		<< avg_quiz2 
		<< setw(10) 
		<< avg_quiz3 
		<< setw(10) 
		<< avg_quiz4 	
		<< endl << endl;

	system("pause");
}