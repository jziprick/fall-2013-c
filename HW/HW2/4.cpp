// Jonathan Ziprick
// ID 2505811
// jziprick1@student.rcc.edu
// HW2

// 4)

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main ()
{

	// Variables
	string name1 = "name";
	string name2 = "another name";
	string food = "food";
	string number = "number";
	string adjective = "adjective";
	string color = "color";
	string animal = "animal";

	// User Input
	cout << "Enter teacher's name.\n";
	cin >> name1;
	cout << "Enter your name.\n";
	cin >> name2;
	cout << "Enter a food.\n";
	cin >> food;
	cout << "Enter a number between 100 and 120.\n";
	cin >> number;
	cout << "Enter an adjective.\n";
	cin >> adjective;
	cout << "Enter a color.\n";
	cin >> color;
	cout << "Enter an animal.\n";
	cin >> animal;

	cout
		<< endl << endl

		<< "Dear " << name1 << ", \n \n"

		<< "I am sorry that I am unable to turn in my home-\n"
		<< "work at this time. First, I ate a rotten " << food << ",\n"
		<< "which made me turn " << color << " and extremely ill. I \n"
		<< "came down with a fever of " << number << ". \n"
		<< "Next, my " << adjective	<< " pet " << animal << " must have \n"
		<< "smelled the remains of the " << food << " on my home- \n"
		<< "work because he ate it. I am currently rewriting \n"
		<< "my homework and hope you will accept it late. \n \n"

		<< "Sincerely, \n"
		<< name2

		<< endl << endl;

	system("pause");
}