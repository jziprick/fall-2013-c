// Jonathan Ziprick
// ID 2505811
// jziprick1@student.rcc.edu
// HW2

// 2)

#include <iostream>
#include <string>

using namespace std;

int main()
{
	string name1 = "Calvin";
	string name2 = "Bill";

	cout << "Hello my name is " << name1 << endl;
	cout << "Hi there " << name1 << "! My name is " << name2 << endl;
	cout << "Nice to meet you " << name2 << ".\n" 
		 << "I am wondering if you're available for what?\n";
	cout << "What? Bye" << name1 << "!\n";
	cout << "Uh... sure, bye " << name2 << "!\n\n";

	cout << name1 << name1 << name1 << name1 << name1 << endl;
	cout << name2 << name2 << name2 << name2 << name2 << endl;

	system("pause");
	return 0;
}

// a)	Hello my name is Calvin
//		Hi there Calvin! My name is Bill
//		Nice to meet you Bill.
//		I am wondering if you're available for what?
//		What? ByeCalvin!
//		Uh... sure, bye Bill!

//		CalvinCalvinCalvinCalvinCalvin
//		BillBillBillBillBill

// b)	9

// c) 	string name1 = "Calvin";
//		string name2 = "Bill";

// d)	cout << "Hello my name is " << name1 << endl;
//		cout << "Hi there " << name1 << "! My name is " << name2 << endl;
//		cout << "Nice to meet you " << name2 << ".\n" 
//				 << "I am wondering if you're available for what?\n";
//		cout << "What? Bye" << name1 << "!\n";
//		cout << "Uh... sure, bye " << name2 << "!\n\n";
//
//		cout << name1 << name1 << name1 << name1 << name1 << endl;
//		cout << name2 << name2 << name2 << name2 << name2 << endl;

// e)	So that the names used throughout the entire program can be changed more quickly and easily.

