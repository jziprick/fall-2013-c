/*
HW 3: Due at 11:59 Friday, September, 20, 2013
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu

Problem 4
*/

#include <iostream>
using namespace std;

int main()
{
	// Variables
	int room = 0;
	int people = 0;

	// User Prompt and Input
	cout << "Enter the room's maximum capacity: ";
	cin >> room;
	cout << "Enter the number of people attending the meeting: ";
	cin >> people;

	// Calculations and Output
	if (people<=room)
		cout << "It is legal to hold the meeting.\n"
			<< "An additional " << room - people << " may legally attend.\n";
	else
		cout << "The meeting cannot be held as planned due to fire regulations.\n"
			<< "At least " << people - room << " must be excluded in order to meet fire regulations.\n";
	system("pause");
}