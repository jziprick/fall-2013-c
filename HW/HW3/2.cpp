/*
HW 3: Due at 11:59 Friday, September, 20, 2013
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu

Problem 2
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
	int apples = 0;
	int oranges = 0;
	int pears = 0;
	int trip = 0;
	cout << "Enter the number of apples: ";
	cin >> apples;
	cout << "Enter the number of oranges: ";
	cin >> oranges;
	cout << "Enter the number of pears: ";
	cin >> pears;

	// Determine which fruit is the least
	if(apples < oranges && apples < pears)
		trip = apples;
	else if(oranges < apples && oranges < pears)
		trip = oranges;
	else
		trip = pears;

	apples = apples - trip;
	oranges = oranges - trip;
	pears = pears - trip;

	// Output
	cout << "The number of apples you should leave: " << apples << endl
	<< "The number of oranges you should leave: " << oranges << endl
	<< "The number of pears you should leave: " << pears << endl;
	
	system("pause");
}