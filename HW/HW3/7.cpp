/*
HW 3: Due at 11:59 Friday, September, 20, 2013
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu

Problem 7
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

int main()
{
	// Variables
	int num1 = 0;
	int num2 = 0;
	int num3 = 0;
	int num4 = 0;
	int num5 = 0;
	int num6 = 0;
	int num7 = 0;
	int num8 = 0;
	int num9 = 0;
	int num10 = 0;
	int pos_sum = 0;
	int neg_sum = 0;
	int all_sum = 0;
	double pos_avg = 0;
	double neg_avg = 0;
	double all_avg = 0;
	int pos_denom = 0;
	int neg_denom = 0;
	int all_denom = 0;
	int pos_neg1 = 0;
	int pos_neg2 = 0;
	int pos_neg3 = 0;
	int pos_neg4 = 0;
	int pos_neg5 = 0;
	int pos_neg6 = 0;
	int pos_neg7 = 0;
	int pos_neg8 = 0;
	int pos_neg9 = 0;
	int pos_neg10 = 0;

	// Random Number Generation
	srand(time(NULL));

	pos_neg1 = rand() % 2;
	pos_neg2 = rand() % 2;
	pos_neg3 = rand() % 2;
	pos_neg4 = rand() % 2;
	pos_neg5 = rand() % 2;
	pos_neg6 = rand() % 2;
	pos_neg7 = rand() % 2;
	pos_neg8 = rand() % 2;
	pos_neg9 = rand() % 2;
	pos_neg10 = rand() % 2;

	if(pos_neg1 == 1)
		num1 = 1 + (rand() % 100);
	else
	{
		num1 = 1 + (rand() % 100);
		num1 -= num1 * 2;
	}

	if(pos_neg2 == 1)
		num2 = 1 + (rand() % 100);
	else
	{
		num2 = 1 + (rand() % 100);
		num2 -= num2 * 2;
	}

	if(pos_neg3 == 1)
		num3 = 1 + (rand() % 100);
	else
	{
		num3 = 1 + (rand() % 100);
		num3 -= num2 * 2;
	}

	if(pos_neg4 == 1)
		num4 = 1 + (rand() % 100);
	else
	{
		num4 = 1 + (rand() % 100);
		num4 -= num1 * 2;
	}

	if(pos_neg5 == 1)
		num5 = 1 + (rand() % 100);
	else
	{
		num5 = 1 + (rand() % 100);
		num5 -= num1 * 2;
	}

	if(pos_neg6 == 1)
		num6 = 1 + (rand() % 100);
	else
	{
		num6 = 1 + (rand() % 100);
		num6 -= num1 * 2;
	}

	if(pos_neg7 == 1)
		num7 = 1 + (rand() % 100);
	else
	{
		num7 = 1 + (rand() % 100);
		num7 -= num1 * 2;
	}

	if(pos_neg8 == 1)
		num8 = 1 + (rand() % 100);
	else
	{
		num8 = 1 + (rand() % 100);
		num8 -= num1 * 2;
	}

	if(pos_neg9 == 1)
		num9 = 1 + (rand() % 100);
	else
	{
		num9 = 1 + (rand() % 100);
		num9 -= num1 * 2;
	}

	if(pos_neg10 == 1)
		num10 = 1 + (rand() % 100);
	else
	{
		num10 = 1 + (rand() % 100);
		num10 -= num1 * 2;
	}

	// Calculations

	if(num1 > 0)
	{
		pos_sum += num1;
		pos_denom ++;
	}
	else
	{
		neg_sum += num1;
		neg_denom ++;
	}

	if(num2 > 0)
	{
		pos_sum += num2;
		pos_denom ++;
	}
	else
	{
		neg_sum += num2;
		neg_denom ++;
	}

	if(num3 > 0)
	{
		pos_sum += num3;
		pos_denom ++;
	}
	else
	{
		neg_sum += num3;
		neg_denom ++;
	}

	if(num4 > 0)
	{
		pos_sum += num4;
		pos_denom ++;
	}
	else
	{
		neg_sum += num4;
		neg_denom ++;
	}

	if(num5 > 0)
	{
		pos_sum += num5;
		pos_denom ++;
	}
	else
	{
		neg_sum += num5;
		neg_denom ++;
	}

	if(num6 > 0)
	{
		pos_sum += num6;
		pos_denom ++;
	}
	else
	{
		neg_sum += num6;
		neg_denom ++;
	}

	if(num7 > 0)
	{
		pos_sum += num7;
		pos_denom ++;
	}
	else
	{
		neg_sum += num7;
		neg_denom ++;
	}

	if(num8 > 0)
	{
		pos_sum += num8;
		pos_denom ++;
	}
	else
	{
		neg_sum += num8;
		neg_denom ++;
	}

	if(num9 > 0)
	{
		pos_sum += num9;
		pos_denom ++;
	}
	else
	{
		neg_sum += num9;
		neg_denom ++;
	}

	if(num10 > 0)
	{
		pos_sum += num10;
		pos_denom ++;
	}
	else
	{
		neg_sum += num10;
		neg_denom ++;
	}
	
	pos_avg = pos_sum / pos_denom;
	neg_avg = neg_sum / neg_denom;
	all_sum = num1 + num2 + num3 + num4 + num5 + num6 + num7 + num8 + num9 + num10;
	all_avg = all_sum / 10;

	cout << "The sum of all the numbers greater than zero is: " << pos_sum << ".\n"
		<< "The average of all numbers greater than zero is: " << pos_avg << ".\n"
		<< "The sum of all numbers zero of less is: " << neg_sum << ".\n"
		<< "The average of all numbers zero or less is: " << neg_avg << ".\n"
		<< "The sum of all numbers, whether positive, negative or zero is: " << all_sum << ".\n"
		<< "The average of all numbers, whether positive, negative or zero is: " << all_avg << ".\n";

	system("pause");
}