/*
HW 3: Due at 11:59 Friday, September, 20, 2013
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu

Problem 5
*/

#include <iostream>
using namespace std;

int main()
{
	// Variables
	int num1 = 0;
	int num2 = 0;
	int num3 = 0;
	int num4 = 0;
	int num5 = 0;
	int num6 = 0;
	int num7 = 0;
	int num8 = 0;
	int num9 = 0;
	int num10 = 0;
	int pos_sum = 0;
	int neg_sum = 0;
	int all_sum = 0;

	// User Prompt and Input
	cout << "Please enter ten whole numbers, negative, positive, or zero. Press return after each number. \n \n";
	cin >> num1 >> num2 >> num3 >> num4 >> num5 >> num6 >> num7 >> num8 >> num9 >> num10;

	// Calculations
	if(num1 > 0)
		pos_sum += num1;
	else
		neg_sum += num1;
	if(num2 > 0)
		pos_sum += num2;
	else
		neg_sum += num2;
	if(num3 > 0)
		pos_sum += num3;
	else
		neg_sum += num3;
	if(num4 > 0)
		pos_sum += num4;
	else
		neg_sum += num4;
	if(num5 > 0)
		pos_sum += num5;
	else
		neg_sum += num5;
	if(num6 > 0)
		pos_sum += num6;
	else
		neg_sum += num6;
	if(num7 > 0)
		pos_sum += num7;
	else
		neg_sum += num7;
	if(num8 > 0)
		pos_sum += num8;
	else
		neg_sum += num8;
	if(num9 > 0)
		pos_sum += num9;
	else
		neg_sum += num9;
	if(num10 > 0)
		pos_sum += num10;
	else
		neg_sum += num10;
	
	all_sum = num1 + num2 + num3 + num4 + num5 + num6 + num7 + num8 + num9 + num10;

	cout << "The sum of all the numbers greater than zero is: " << pos_sum << ".\n"
		<< "The sum of all numbers less than zero is: " << neg_sum << ".\n"
		<< "The sum of all numbers, whether positive, negative or zero is: " << all_sum << ".\n";

	system("pause");
}