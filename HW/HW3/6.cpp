/*
HW 3: Due at 11:59 Friday, September, 20, 2013
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu

Problem 6
*/

#include <iostream>
using namespace std;

int main()
{
	// Variables
	int num1 = 0;
	int num2 = 0;
	int num3 = 0;
	int num4 = 0;
	int num5 = 0;
	int num6 = 0;
	int num7 = 0;
	int num8 = 0;
	int num9 = 0;
	int num10 = 0;
	int pos_sum = 0;
	int neg_sum = 0;
	int all_sum = 0;
	double pos_avg = 0;
	double neg_avg = 0;
	double all_avg = 0;
	int pos_denom = 0;
	int neg_denom = 0;
	int all_denom = 0;

	// User Prompt and Input
	cout << "Please enter ten whole numbers, negative, positive, or zero. Press return after each number. \n \n";
	cin >> num1 >> num2 >> num3 >> num4 >> num5 >> num6 >> num7 >> num8 >> num9 >> num10;


	// Calculations

	if(num1 > 0)
	{
		pos_sum += num1;
		pos_denom ++;
	}
	else
	{
		neg_sum += num1;
		neg_denom ++;
	}

	if(num2 > 0)
	{
		pos_sum += num2;
		pos_denom ++;
	}
	else
	{
		neg_sum += num2;
		neg_denom ++;
	}

	if(num3 > 0)
	{
		pos_sum += num3;
		pos_denom ++;
	}
	else
	{
		neg_sum += num3;
		neg_denom ++;
	}

	if(num4 > 0)
	{
		pos_sum += num4;
		pos_denom ++;
	}
	else
	{
		neg_sum += num4;
		neg_denom ++;
	}

	if(num5 > 0)
	{
		pos_sum += num5;
		pos_denom ++;
	}
	else
	{
		neg_sum += num5;
		neg_denom ++;
	}

	if(num6 > 0)
	{
		pos_sum += num6;
		pos_denom ++;
	}
	else
	{
		neg_sum += num6;
		neg_denom ++;
	}

	if(num7 > 0)
	{
		pos_sum += num7;
		pos_denom ++;
	}
	else
	{
		neg_sum += num7;
		neg_denom ++;
	}

	if(num8 > 0)
	{
		pos_sum += num8;
		pos_denom ++;
	}
	else
	{
		neg_sum += num8;
		neg_denom ++;
	}

	if(num9 > 0)
	{
		pos_sum += num9;
		pos_denom ++;
	}
	else
	{
		neg_sum += num9;
		neg_denom ++;
	}

	if(num10 > 0)
	{
		pos_sum += num10;
		pos_denom ++;
	}
	else
	{
		neg_sum += num10;
		neg_denom ++;
	}
	
	pos_avg = pos_sum / pos_denom;
	neg_avg = neg_sum / neg_denom;
	all_sum = num1 + num2 + num3 + num4 + num5 + num6 + num7 + num8 + num9 + num10;
	all_avg = all_sum / 10;

	cout << "The sum of all the numbers greater than zero is: " << pos_sum << ".\n"
		<< "The average of all numbers greater than zero is: " << pos_avg << ".\n"
		<< "The sum of all numbers zero of less is: " << neg_sum << ".\n"
		<< "The average of all numbers zero or less is: " << neg_avg << ".\n"
		<< "The sum of all numbers, whether positive, negative or zero is: " << all_sum << ".\n"
		<< "The average of all numbers, whether positive, negative or zero is: " << all_avg << ".\n";

	system("pause");
}