/*
HW 3: Due at 11:59 Friday, September, 20, 2013
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu

Problem 3
*/

#include <iostream>
using namespace std;

int main()
{
	// Variables
	int cereal_ounces = 1;
	double cereal_ton = 0;
	double ounces_per_ton = 35273.92;
	int cereal_boxes = 0;

	// User Input
	while(cereal_ounces != 0)
	{
		cout << "Enter weight in ounces of cereal package. Enter the 0 to quit: ";
		cin >> cereal_ounces;
		cereal_ton = cereal_ounces / ounces_per_ton;
		cereal_boxes = ounces_per_ton / cereal_ounces;
		cout << "The cereal package's weight in metric ton is: " << cereal_ton << endl
			<< "The number of boxes need to yield 1 metric ton of cereal is: " << cereal_boxes << endl;
	}
}