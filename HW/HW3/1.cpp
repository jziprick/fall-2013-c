/*
HW 3: Due at 11:59 Friday, September, 20, 2013
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu

Problem 1
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
	// Variables
	int dash = 0;
	string num;

	cout << "Enter a telephone number: ";
	cin >> num;
	dash = num.find("-");
	num.replace(dash, 1, "");
	cout << num << endl << endl;

	system("pause");
}