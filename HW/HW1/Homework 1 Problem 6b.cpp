// Jonathan Ziprick
// CIS-5: Programming Concepts and Methodology I:C++
// Professor Ung
// Homework 1, Problem 6
// 9/4/13

// b.

#include iostream>														// deleted < symbol - causes various compile errors: 
																		//		"Error: expected a file name"
																													
using namespace std;													//		"Error: name must be a namespace name"
int main()
{
int number_1 = 0;														
int number_2 = 0;														
cout << "Hello.\n";														//		"Error: identifier 'cout' is undefined
cout << "Please enter Number 1: " << endl;								//		"Error: identifier 'endl' is undefined
cin >> number_1;														//		"Error: identifier 'cin' is undefined													
cout << "Now please enter Number 2: " << endl;
cin >> number_2;														
cout << "The sum of the numbers is " << number_1 + number_2 << ".\n";	
cout << "The product of numbers is " << number_1 * number_2 << ".\n";	
system("pause");
return 0;
}