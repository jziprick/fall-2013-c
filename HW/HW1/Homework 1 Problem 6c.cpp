// Jonathan Ziprick
// CIS-5: Programming Concepts and Methodology I:C++
// Professor Ung
// Homework 1, Problem 6
// 9/4/13

// c.

#include <iostream>														
																		
																													
using namespace std;													
main()																// deleted "int" from int main() - causes compile error: "error C4430: missing type specifier - int assumed."
{
int number_1 = 0;														
int number_2 = 0;														
cout << "Hello.\n";														
cout << "Please enter Number 1: " << endl;								
cin >> number_1;																											
cout << "Now please enter Number 2: " << endl;
cin >> number_2;														
cout << "The sum of the numbers is " << number_1 + number_2 << ".\n";	
cout << "The product of numbers is " << number_1 * number_2 << ".\n";	
system("pause");
return 0;
}