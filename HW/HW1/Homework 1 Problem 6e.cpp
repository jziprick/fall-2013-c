// Jonathan Ziprick
// CIS-5: Programming Concepts and Methodology I:C++
// Professor Ung
// Homework 1, Problem 6
// 9/4/13

// e.

#include <iostream>														
																		
																													
using namespace std;													
int main																// omitted of the () and then both
																		// error C2059: syntax error : ')'
																		// error C2470: 'main' : looks like a function definition, but there is no parameter list; skipping apparent body
{
int number_1 = 0;														
int number_2 = 0;														
cout << "Hello.\n";														
cout << "Please enter Number 1: " << endl;								
cin >> number_1;																											
cout << "Now please enter Number 2: " << endl;
cin >> number_2;														
cout << "The sum of the numbers is " << number_1 + number_2 << ".\n";	
cout << "The product of numbers is " << number_1 * number_2 << ".\n";	
system("pause");
return 0;
}