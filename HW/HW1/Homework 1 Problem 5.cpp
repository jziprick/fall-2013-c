// Jonathan Ziprick
// CIS-5: Programming Concepts and Methodology I:C++
// Professor Ung
// Homework 1, Problem 5
// 9/4/13

#include <iostream>
using namespace std;
int main()
{
int number_1 = 0;														// 1st integer declaration and initialization
int number_2 = 0;														// 2nd integer declaration and initialization
cout << "Hello.\n";
cout << "Please enter Number 1: " << endl;
cin >> number_1;														// user's input for 1st integer
cout << "Now please enter Number 2: " << endl;
cin >> number_2;														// user's input for 2nd integer
cout << "The sum of the numbers is " << number_1 + number_2 << ".\n";	// add integers and output the sum
cout << "The product of numbers is " << number_1 * number_2 << ".\n";	// multipy integers and output the product
return 0;
}