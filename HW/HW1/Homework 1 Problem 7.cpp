// Jonathan Ziprick
// CIS-5: Programming Concepts and Methodology I:C++
// Professor Ung
// Homework 1, Problem 78
// 9/4/13

// e.

#include <iostream>														
																		
																													
using namespace std;													
int main()																
{
cout << "**************************************************" << endl;
cout << endl;
cout << "              C C C             S S S S       !!" << endl;
cout << "             C      C         S         S     !!" << endl;
cout << "            C                S                !!" << endl;
cout << "           C                  S               !!" << endl;
cout << "           C                   S S S S        !!" << endl;
cout << "           C                            S     !!" << endl;
cout << "            C                            S    !!" << endl;
cout << "              C     C          S        S       " << endl;
cout << "               C C C             S S S S      00" << endl;
cout << endl;                                      
cout << "**************************************************" << endl;
cout << endl;
cout << "           Computer Science is Cool Stuff!!!" << endl << endl;

system("pause");
return 0;
}