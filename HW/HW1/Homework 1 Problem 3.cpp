// Jonathan Ziprick
// CIS-5: Programming Concepts and Methodology I:C++
// Professor Ung
// Homework 1, Problem 3
// 9/4/13

#include <iostream>

using namespace std;

int main( )
{
int number_of_pods, peas_per_pod, total_peas;
cout << "Hello.\n";										// added hello message
cout << "Press return after entering a number.\n";
cout << "Enter the number of pods:\n";
cin >> number_of_pods;
cout << "Enter the number of peas in a pod:\n";
cin >> peas_per_pod;
total_peas = number_of_pods / peas_per_pod;				// changed multiplication sign to division sign
cout << "If you have ";
cout << number_of_pods;
cout << " pea pods\n";
cout << "and ";
cout << peas_per_pod;
cout << " peas in each pod, then\n";
cout << "you have ";
cout << total_peas;
cout << " peas in all the pods.\n";
cout << "Good-bye.\n";									// added good-bye message
system ("pause");
return 0;
}