/*
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu
HW4
Problem: 2
Due: 11:59 Friday, September 27, 2013
I certify this is my own work and code
*/

#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Variables
	int lbs = 0;
	int inches = 0;
	int age = 0;
	int choco_bar = 0;
	int choco_cal = 230;
	double bmr = 0;
	string gender;
	bool loop = true;

	// User Input
	while(loop==true)
	{
		cout << "Enter your weight in pounds. \n \n";
		cin >> lbs;

		cout << endl << "Enter your height in inches. \n \n";
		cin >> inches;

		cout << endl << "Enter your age in years. \n \n";
		cin >> age;

		cout << endl << "Are you male (M) or female (F)? \n \n";
		cin >> gender;

		// Output Based on Gender
		if(gender=="M" || gender=="m" || gender=="Male" || gender=="male")
		{
			bmr = 66 + (6.3 * static_cast<double>(lbs)) + (12.9 * static_cast<double>(inches)) - (6.8 * static_cast<double>(age));
			choco_bar = bmr / choco_cal;
			cout << endl << "You should consume " << choco_bar << " chocolate bars to maintain your weight. \n \n";
		}

		else if(gender=="F" || gender=="f" || gender=="Female" || gender=="female")
		{
			bmr = 655 + (4.3 * lbs) + (4.7 * inches) - (4.7 * age);
			choco_bar = bmr / choco_cal;
			cout << endl << "You should consume " << choco_bar << " chocolate bars to maintain your weight. \n \n";
		}

		else
			cout << endl << "You did not answer whether M or F. \n \n";
	}
}