/*
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu
HW4
Problem: 3
Due: 11:59 Friday, September 27, 2013
I certify this is my own work and code
*/

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
	// Variables
	bool loop = true;
	int n = 0;
	int score = 0;
	int total = 0;
	int score_sum = 0;
	int total_sum = 0;
	double grade = 0;
	int i = 0;

	while(loop==true)
	{
		// Input Number of Exercises
		cout << "How many exercises to input? ";
		cin >> n;
		cout << endl;
		
		// Input Scores and Total Based on Number of Exercises
		for(i = 0; i < n; i++)
		{
			cout << "Score received for exercise " << 1 + i << ": ";
			cin >> score;
			cout << "Total points possible for exercise " << 1 + i << ": ";
			cin >> total;
			score_sum += score;
			total_sum += total;
			cout << endl;
		}

		//Caculate Grade
		grade = (static_cast<double>(score_sum) / total_sum) * 100;
		cout << "Your total is " << score_sum << " out of " << total_sum << ", or " << fixed << setprecision(2) << grade << "%. \n \n";	

		cout << "Do you wish to quit? (Y/N)";
		char quit;
		cin >> quit;

		if(quit=='Y' || quit=='y')
			loop = false;
		else
			loop = true;
	}
}