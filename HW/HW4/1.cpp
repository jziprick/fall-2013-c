/*
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu
HW4
Problem: 1
Due: 11:59 Friday, September 27, 2013
I certify this is my own work and code
*/

#include <iostream>
#include <string>

using namespace std;

int main()
{
	string num = "num";

	bool loop = true;

	while(loop==true)
	{
		cout << "Enter a year between 1000 and 3000 written as \n"
			<< "a four-digit Arabic (ordinary) numeral. You may \n"
			<< "quit at any time by entering 'Q' or 'q'. \n \n";
		getline(cin, num);
		
		if(num=="q" || num=="Q")
			loop = false;

		else
		{
			cout << "That number written in Roman numerals is: ";

			// Break Apart Number
			string num1000 = num.substr(0,1);
			string num0100 = num.substr(1,1);
			string num0010 = num.substr(2,1);
			string num0001 = num.substr(3,1);			

			// Assign Roman Numerals
			if(num1000=="1")
				cout << "M";
			else if(num1000=="2")
				cout << "MM";
			else if(num1000=="3")
				cout << "MMM";
			else
				cout << "_";

			if(num0100=="1")
				cout << "C";
			else if(num0100=="2")
				cout << "CC";
			else if(num0100=="3")
				cout << "CCC";
			else if(num0100=="4")
				cout << "CD";
			else if(num0100=="5")
				cout << "D";
			else if(num0100=="6")
				cout << "DC";
			else if(num0100=="7")
				cout << "DCC";
			else if(num0100=="8")
				cout << "DCCC";
			else if(num0100=="9")
				cout << "CM";
			else if(num0100=="0")
				cout << "";
			else
				cout << "_";

			if(num0010=="1")
				cout << "X";
			else if(num0010=="2")
				cout << "XX";
			else if(num0010=="3")
				cout << "XXX";
			else if(num0010=="4")
				cout << "XL";
			else if(num0010=="5")
				cout << "L";
			else if(num0010=="6")
				cout << "LX";
			else if(num0010=="7")
				cout << "LXX";
			else if(num0010=="8")
				cout << "LXXX";
			else if(num0010=="9")
				cout << "XC";
			else if(num0010=="0")
				cout << "";
			else
				cout << "_";

			if(num0001=="1")
				cout << "I";
			else if(num0001=="2")
				cout << "II";
			else if(num0001=="3")
				cout << "III";
			else if(num0001=="4")
				cout << "IV";
			else if(num0001=="5")
				cout << "V";
			else if(num0001=="6")
				cout << "VI";
			else if(num0001=="7")
				cout << "VII";
			else if(num0001=="8")
				cout << "VIII";
			else if(num0001=="9")
				cout << "IX";
			else if(num0001=="0")
				cout << "";
			else
				cout << "_";

			cout << endl << endl;
		}
	}
}