/*
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu
HW4
Problem: 7
Due: 11:59 Friday, September 27, 2013
I certify this is my own work and code
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <time.h>

using namespace std;

int main()
{
	// Variables
	bool loop = true;
	int ai_1 = 0;
	int ai_2 = 0;
	char quit;
	bool end_loop = true;
	char choice;
	int game = 0;
	int ai = 0;
	int select = 0;

	cout << "Welcome to rock-paper-scissors! \n"
		<< "Select an option:\n\n"
		<< "1\tPlayer vs. AI\n"
		<< "2\tAI vs. AI\n\n";

	cin >> select;
	cout << endl << endl;


if(select == 1)
{
	while(loop==true)
	{
		cout << "Welcome to rock-paper-scissors! \n";
		cout << "Choose rock (R), paper (P), or scissors (S). \n";
		cin >> choice;
		srand (time(NULL));
		ai = (rand() % 3) + 1;

			// AI CHOICE == ROCK
			if(ai==1)
			{
				// USER CHOOSES ROCK
				if(choice=='r' || choice=='R')
					cout << "You chose rock; AI chose rock. Rock does nothing to rock! Nobody wins.";

				// USER CHOOSES PAPER
				else if(choice=='p' || choice=='P')
					cout << "You chose paper; AI chose rock. Paper covers rock! YOU WIN!";

				// USER CHOOSES SCISSORS
				else if(choice=='s' || choice=='S')
					cout << "You chose scissors; AI chose scissors. Rock breaks scissors! YOU LOSE!";

				else
					cout << "Invalid choice.";
			}
		
			// AI CHOICE == PAPER
			else if(ai==2)
			{
				// USER CHOOSES ROCK
				if(choice=='r' || choice=='R')
					cout << "You chose rock; AI chose paper. Paper covers rock! YOU LOSE!";

				// USER CHOOSES PAPER
				else if(choice=='p' || choice=='P')
					cout << "You chose paper; AI chose paper. Paper does nothing to paper! Nobody wins.";

				// USER CHOOSES SCISSORS
				else if(choice=='s' || choice=='S')
					cout << "You chose scissors; AI chose paper. Scissors cuts paper! YOU WIN!";

				else
					cout << "Invalid choice.";
			}

			// AI CHOICE == SCISSORS
			else
			{
				// USER CHOOSES ROCK
				if(choice=='r' || choice=='R')
					cout << "You chose rock; AI chose scissors. Rock breaks scissors! YOU WIN!";

				// USER CHOOSES PAPER
				else if(choice=='p' || choice=='P')
					cout << "You chose paper; AI chose scissors. Scissors cuts paper! YOU LOSE!";

				// USER CHOOSES SCISSORS
				else if(choice=='s' || choice=='S')
					cout << "You chose scissors; AI chose scissors. Scissors does nothing to paper! Nobody wins.";

				else
					cout << "Invalid choice.";
			}

		cin.ignore();
		cin.get();

		cout << "Do you wish to quit? (Y/N)";
		char quit;
		cin >> quit;

		if(quit=='Y' || quit=='y')
			loop = false;
		else
			loop = true; 
	}
}


else if(select == 2)
{
	while(loop==true)
	{
		srand (time(NULL));
		ai_1 = (rand() % 3);
		ai_2 = (rand() % 3);

			// AI CHOICE == ROCK
			if(ai_1 == 0)
			{
				// AI2 CHOOSES ROCK
				if(ai_2 == 0)
					cout << "AI_1 chose rock; AI_2 chose rock. Rock does nothing to rock! Nobody wins.\n\n";

				// AI2 CHOOSES PAPER
				else if(ai_2 == 1)
					cout << "AI_1 chose rock; AI_2 chose paper. Paper covers rock! AI_2 wins!\n\n";

				// AI2 CHOOSES SCISSORS
				else
					cout << "AI_1 chose rock; AI_2 chose scissors. Rock breaks scissors! AI_1 wins!\n\n";
			}
		
			// AI CHOICE == PAPER
			else if(ai_1 == 1)
			{
				// AI2 CHOOSES ROCK
				if(ai_2 == 0)
					cout << "AI_1 chose paper; AI_2 chose rock. Paper covers rock! AI_1 wins!\n\n";

				// AI2 CHOOSES PAPER
				else if(ai_2 == 1)
					cout << "AI_1 chose paper; AI_2 chose paper. Paper does nothing to paper! Nobody wins.\n\n";

				// AI2 CHOOSES SCISSORS
				else
					cout << "AI_1 chose paper; AI_2 chose scissors. Scissors cuts paper! AI_2 wins!\n\n";
			}

			// AI CHOICE == SCISSORS
			else
			{
				// AI2 CHOOSES ROCK
				if(ai_2 == 0)
					cout << "AI_1 chose scissors; AI_2 chose rock. Rock breaks scissors! AI_2 wins!\n\n";

				// AI2 CHOOSES PAPER
				else if(ai_2 == 1)
					cout << "AI_1 chose scissors; AI_2 chose paper. Scissors cuts paper! AI_1 wins!\n\n";

				// AI2 CHOOSES SCISSORS
				else
					cout << "AI_1 chose scissors; AI_2 chose scissors. Scissors does nothing to paper! Nobody wins.\n\n";
			}
		
		end_loop = true;
		while(end_loop)
		{
			cout << "Play again? (Y/N)\n\n";
			cin >> quit;
			cout << endl;
			if(quit=='Y' || quit=='y')
			{
				loop = true;
				end_loop = false;
			}
			else if(quit=='N' || quit=='n')
			{
				loop = false;
				end_loop = false;
			}
			else
				cout << "Invalid input.\n\n";
		}
	}
}


}