/*
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu
HW4
Problem: 6
Due: 11:59 Friday, September 27, 2013
I certify this is my own work and code
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <time.h>

using namespace std;

int main()
{
	// Variables
	bool loop = true;
	int ai_1 = 0;
	int ai_2 = 0;
	char quit;
	bool end_loop = true;
	char choice;
	int game = 0;

	cout << "Welcome to rock-paper-scissors! \n"
		<< "Select an option:\n\n";


	while(loop==true)
	{
		srand (time(NULL));
		ai_1 = (rand() % 3);
		ai_2 = (rand() % 3);

			// AI CHOICE == ROCK
			if(ai_1 == 0)
			{
				// USER CHOOSES ROCK
				if(ai_2 == 0)
					cout << "AI_1 chose rock; AI_2 chose rock. Rock does nothing to rock! Nobody wins.\n\n";

				// USER CHOOSES PAPER
				else if(ai_2 == 1)
					cout << "AI_1 chose rock; AI_2 chose paper. Paper covers rock! AI_2 wins!\n\n";

				// USER CHOOSES SCISSORS
				else
					cout << "AI_1 chose rock; AI_2 chose scissors. Rock breaks scissors! AI_1 wins!\n\n";
			}
		
			// AI CHOICE == PAPER
			else if(ai_1 == 1)
			{
				// USER CHOOSES ROCK
				if(ai_2 == 0)
					cout << "AI_1 chose paper; AI_2 chose rock. Paper covers rock! AI_1 wins!\n\n";

				// USER CHOOSES PAPER
				else if(ai_2 == 1)
					cout << "AI_1 chose paper; AI_2 chose paper. Paper does nothing to paper! Nobody wins.\n\n";

				// USER CHOOSES SCISSORS
				else
					cout << "AI_1 chose paper; AI_2 chose scissors. Scissors cuts paper! AI_2 wins!\n\n";
			}

			// AI CHOICE == SCISSORS
			else
			{
				// USER CHOOSES ROCK
				if(ai_2 == 0)
					cout << "AI_1 chose scissors; AI_2 chose rock. Rock breaks scissors! AI_2 wins!\n\n";

				// USER CHOOSES PAPER
				else if(ai_2 == 1)
					cout << "AI_1 chose scissors; AI_2 chose paper. Scissors cuts paper! AI_1 wins!\n\n";

				// USER CHOOSES SCISSORS
				else
					cout << "AI_1 chose scissors; AI_2 chose scissors. Scissors does nothing to paper! Nobody wins.\n\n";
			}
		
		end_loop = true;
		while(end_loop)
		{
			cout << "Play again? (Y/N)\n\n";
			cin >> quit;
			cout << endl;
			if(quit=='Y' || quit=='y')
			{
				loop = true;
				end_loop = false;
			}
			else if(quit=='N' || quit=='n')
			{
				loop = false;
				end_loop = false;
			}
			else
				cout << "Invalid input.\n\n";
		}
	}
}