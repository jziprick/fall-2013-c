/*
Name: Jonathan Ziprick
ID: 2505811
Email: jziprick1@student.rcc.edu
HW4
Problem: 3
Due: 11:59 Friday, September 27, 2013
I certify this is my own work and code
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <time.h>

using namespace std;

int main()
{
	// Variables
	bool loop = true;
	char choice;
	int game = 0;
	int ai = 0;

	while(loop==true)
	{
		cout << "Welcome to rock-paper-scissors! \n";
		cout << "Choose rock (R), paper (P), or scissors (S). \n";
		cin >> choice;
		srand (time(NULL));
		ai = (rand() % 3) + 1;

			// AI CHOICE == ROCK
			if(ai==1)
			{
				// USER CHOOSES ROCK
				if(choice=='r' || choice=='R')
					cout << "You chose rock; AI chose rock. Rock does nothing to rock! Nobody wins.";

				// USER CHOOSES PAPER
				else if(choice=='p' || choice=='P')
					cout << "You chose paper; AI chose rock. Paper covers rock! YOU WIN!";

				// USER CHOOSES SCISSORS
				else if(choice=='s' || choice=='S')
					cout << "You chose scissors; AI chose scissors. Rock breaks scissors! YOU LOSE!";

				else
					cout << "Invalid choice.";
			}
		
			// AI CHOICE == PAPER
			else if(ai==2)
			{
				// USER CHOOSES ROCK
				if(choice=='r' || choice=='R')
					cout << "You chose rock; AI chose paper. Paper covers rock! YOU LOSE!";

				// USER CHOOSES PAPER
				else if(choice=='p' || choice=='P')
					cout << "You chose paper; AI chose paper. Paper does nothing to paper! Nobody wins.";

				// USER CHOOSES SCISSORS
				else if(choice=='s' || choice=='S')
					cout << "You chose scissors; AI chose paper. Scissors cuts paper! YOU WIN!";

				else
					cout << "Invalid choice.";
			}

			// AI CHOICE == SCISSORS
			else
			{
				// USER CHOOSES ROCK
				if(choice=='r' || choice=='R')
					cout << "You chose rock; AI chose scissors. Rock breaks scissors! YOU WIN!";

				// USER CHOOSES PAPER
				else if(choice=='p' || choice=='P')
					cout << "You chose paper; AI chose scissors. Scissors cuts paper! YOU LOSE!";

				// USER CHOOSES SCISSORS
				else if(choice=='s' || choice=='S')
					cout << "You chose scissors; AI chose scissors. Scissors does nothing to paper! Nobody wins.";

				else
					cout << "Invalid choice.";
			}

		cin.ignore();
		cin.get();

		cout << "Do you wish to quit? (Y/N)";
		char quit;
		cin >> quit;

		if(quit=='Y' || quit=='y')
			loop = false;
		else
			loop = true; 
	}
}