#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	int com_num = 0;
	int user_num = 0;
	int i = 0;

	srand(time(NULL));

	com_num = 1 + (rand() % 100);

	cout << "This is a guessing game.\n"
		<< "The computer has randomly selected a number between 1 and 100.\n"
		<< "Select a number between 1 and 100 and try to guess the computer's number within 10 guesses. \n \n";
	
//	cout << "Number is: " << com_num << endl << endl;

	for(i=0; i<10; i++)
	{
		cin >> user_num;
		if(user_num<com_num)
			cout << "You selected a number too small! \n \n";
		else if(user_num>com_num)
			cout << "You selected a number too large! \n \n";
		else
		{
			cout << "You guessed the number!";
			break;
		}
	}
	
	if(user_num==com_num)
		cout << endl << endl << "YOU WIN!";
	else
		cout << "GAME OVER!";

	cout << endl << endl;

	system("pause");
}