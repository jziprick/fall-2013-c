#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
	double guess = 2;
	double prev_guess = 2;
	int n = 0;
	double r = 0;

	cout << "This program will compute the square root\n";
	cout << "of a number using the Babylonian algorithm.\n";
	cout << "Please enter a whole number and press the return key:\n";
	cin >> n;
	guess = n / 2;
//	cout << "guess = " << guess << endl << endl;
	do
	{
		prev_guess = guess;
//		cout << "prev_guess = " << prev_guess << endl << endl;
		r = static_cast<double>(n) / guess;
//		cout << "r = n / guess \n r = " << r << endl << endl;
		guess = (guess + r) / 2;
//		cout << "guess = (guess + r) / 2" << endl << endl;
//		cout << "guess = " << guess << endl << endl;
	}
	while((guess/100) < (prev_guess/100));

	cout << fixed;
	cout << setprecision(5) << guess;
	cout << endl << endl;

	system("pause");
}