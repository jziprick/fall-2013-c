#include <iostream>
#include <string>

using namespace std;

int main()
{
	string grade;
	bool loop = true;

	while(loop==true)
	{
		cout << "Please input a grade. Enter 'q' or 'Q' to quit. \n \n";
		getline(cin, grade);
		string letter = grade.substr(0,1);
		string pos_min = grade.substr(1,1);
		
		if(letter=="q" || letter=="Q")
			loop = false;

		else if(letter=="A")
			if(pos_min=="+")
				cout << "4.0";
			else if(pos_min=="-")
				cout << "3.7";
			else
				cout << "4.0";
		else if(letter=="B")
			if(pos_min=="+")
				cout << "3.3";
			else if(pos_min=="")
				cout << "3.0";
			else
				cout << "2.7";
		else
			cout << "Enter a valid entry.";

		cout << endl << endl;
	}
}