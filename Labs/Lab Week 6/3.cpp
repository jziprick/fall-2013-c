#include <iostream>
#include <string>

int strLength(std::string str)
{
	int strLength = str.length();
	return strLength;
}

int main()
{
	std::string str;
	std::cout << "Enter string: ";
	std::cin >> str;
	std::cout << strLength(str);

	std::cin.ignore();
	std::cin.get();
}