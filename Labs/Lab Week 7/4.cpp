#include <iostream>
#include <iomanip>

bool leap_year (int year)
{
	bool leap = false;

	if((year % 4 == 0 && year % 100 != 0) || ( year % 400 == 0))
		leap = true;
	else
		leap = false;

	return leap;
}

int main ()
{
	bool running = true;
	while (running)
	{
		int year = 0;
		std::cout << "Enter a year: ";
		std::cin >> year;
		switch (leap_year (year))
		{
		case true:
			std::cout << "This is a leap year.\n\n";
			break;
		case false:
			std::cout << "This is not a leap year.\n\n";
			break;
		}
	}
}