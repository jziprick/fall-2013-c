#include <iostream>
#include <iomanip>

void power (double num, int expo, double& result)
{
	result = num;
	for(int i = 0; i < expo - 1; i++)
	{
		result = result * num;
	}
}

void power (double num, double expo, double& result)
{
	result = num;
	for(int i = 0; i < expo - 1; i++)
	{
		result = result * num;
	}
}

int main ()
{
	double num = 0;
	int expo = 0;
	double result = 0;

	std::cout << "Enter number: ";
	std::cin >> num;
	std::cout << "Enter power for number: ";
	std::cin >> expo;
	power (num, expo, result);
	std::cout << num << " to the " << expo << " power equals " << std::setprecision(5) << result;
	std::cin.ignore ();
	std::cin.get ();
}