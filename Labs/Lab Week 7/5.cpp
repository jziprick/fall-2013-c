#include <iostream>

int factorial (int num)
{
	int x1 = num;
	int x2 = num - 1;
	
	for (int i = 0; i < num; i ++)
	{
		x1 *= x2;
		x2 --;
	}

	return x1;
}

int main ()
{
	bool running = true;
	int num = 0;
	while (running)
	{
		std::cout << "Enter factorial: ";
		std::cin >> num;
		std::cout << "Product: " << factorial (num) << "\n\n";
	}
}