#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
	bool game_loop = true;		// run game loop
	bool user_loop = true;		// run user loop
	bool ai_loop = true;		// run ai loop
	bool end_loop = true;		// run end loop
	int toothpicks = 23;		// remaining toothpicks
	int user = 0;				// toothpicks that user takes
	int ai = 0;					// toothpicks that ai takes
	bool win = false;			// win or lose
	char replay;				// play again or quit


	while(game_loop)
	{
		// USER SELECTION
		do
		{
			cout << "There are " << toothpicks << " toothpicks left.\n";
			cout << "Take 1, 2, or 3 toothpicks.\n";
			cin >> user;
			cout << endl;

			if(user > 3 || user < 1)
			{
				cout << user << " is not a valid selection. \n";
				cout << "Please select again. \n";
			}

			else if(user > toothpicks)
			{
				cout << user << " is more than the " << toothpicks << "remaining. \n";
				cout << "Please select again. \n";
			}

			else if(user == 1 || user == 2 || user == 3)
			{
				cout << "You took " << user << " from the " << toothpicks << " remaining toothpicks.\n";
				toothpicks -= user;
				cout << "There are now " << toothpicks << " remaining toothpicks.\n\n";
				user_loop = false;

				if(toothpicks == 0)
				{
					ai_loop = false;
					user_loop = false;
				}

				else
					cout << "AI's turn:\n \n";
			}

			else
			{
				cout << user << " is not a valid selection. \n";
				cout << "Please select again. \n";
			}
		}

		while(user_loop);

		// AI SELECTION

		while(ai_loop)
		{
			if(toothpicks > 4)
			{
				ai = 4 - user;
				cout << "AI took " << ai << " from the " << toothpicks << " remaining toothpicks.\n";
				toothpicks -= ai;
				cout << "There are now " << toothpicks << " remaining toothpicks.\n\n";
			}
			else if(toothpicks == 4)
			{
				ai = 3;
				cout << "AI took " << ai << " from the " << toothpicks << " remaining toothpicks.\n";
				toothpicks-= ai;
				cout << "There are now " << toothpicks << " remaining toothpicks.\n\n";
			}
			else if(toothpicks == 3)
			{
				ai = 2;
				cout << "AI took " << ai << " from the " << toothpicks << " remaining toothpicks.\n";
				toothpicks-= ai;
				cout << "There are now " << toothpicks << " remaining toothpicks.\n\n";
			}
			else if(toothpicks == 2)
			{
				ai = 1;
				cout << "AI took " << ai << " from the " << toothpicks << " remaining toothpicks.\n";
				toothpicks-= ai;
				cout << "There are now " << toothpicks << " remaining toothpicks.\n\n";
			}
			else
			{
				ai = 1;
				cout << "AI took " << ai << " from the " << toothpicks << " remaining toothpicks.\n";
				toothpicks-= ai;
				cout << "There are now " << toothpicks << " remaining toothpicks.\n\n";
			}

			if(toothpicks == 0)
			{
				ai_loop = false;
				user_loop = false;
				win = true;
			}

			else
			{
				ai_loop = false;
				cout << "Your turn:\n \n";
			}
		}

		if(toothpicks == 0 && win == true)
		{
			cout << "YOU WIN!!!\n\n";
			cout << "Do you want to play again? (Y/N)";
			cin >> replay;
			while(end_loop)
				if(replay == 'Y' || replay == 'y')
				{
					game_loop = true;
					end_loop = false;
				}
				else if(replay == 'N' || replay == 'n')
				{
					game_loop = false;
					end_loop = false;
				}
				else
				{
					cout << "Invalid entry.\n\n";
					end_loop = true;
				}
		}
		else if(toothpicks == 0 && win == false)
		{
			cout << "You lose!\n\n";
			cout << "Do you want to play again? (Y/N)";
			cin >> replay;
			while(end_loop)
				if(replay == 'Y' || replay == 'y')
				{
					game_loop = true;
					end_loop = false;
				}
				else if(replay == 'N' || replay == 'n')
				{
					game_loop = false;
					end_loop = false;
				}
				else
				{
					cout << "Invalid entry.\n\n";
					end_loop = true;
				}
		}
		else
		{
			user_loop = true;
			ai_loop = true;
		}
	}
}