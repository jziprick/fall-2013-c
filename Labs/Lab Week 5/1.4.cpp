#include <iostream>
#include <string>

// MAIN FUNCTION
int main()
{
	std::string word;
	std::string reverse_word;
	bool loop = true;

	while(loop)
	{
		std::cout << "Enter a word: ";
		std::cin >> word;
		for(int i = word.length()-1;i>=0;i--)
		{
			reverse_word += word[i];
		}
		
		std::cout << std::endl << word << std::endl << std::endl;
		std::cout << reverse_word << std::endl << std::endl;

		
		if(word == reverse_word)
		{
			std::cout << "This word is a palindrome.";
		}
		else
		{
			std::cout << "This word is not a palindrome.";
		}
		
		reverse_word = "";
		std::cout << std::endl << std::endl;

	}
}

