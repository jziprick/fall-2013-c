#include <iostream>

int main()
{
        int stars = 0;
        int rows = 0;

		std::cout << "Enter the number of stars: ";
		std::cin >> stars;
		std::cout << std::endl << std::endl;

        rows = stars / 2;
        
        // ROWS
        for(int i = 0; i < rows; i++)
        {
                // STARS
                for(double j = 0; j < stars; j++)
                {
					std::cout << "*";

					if(j / 2 == i)
						std::cout << "-";
                }

				std::cout << std::endl;
        }

	std::cin.ignore();
	std::cin.get();
}