#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
	// Variables
	string input = "input";
	double balance = 0;			// total balance
	double thousand = 1000;		// $1000 balance at 1.5% interest rate
	double rate = 0.015;		// 1.5% interest rate
	double rate_alt = 0.01;		// 1.0% interest rate
	double interest = 0;		// 1.5% interest amount
	double interest_alt = 0;	// 1.0 interest amount
	double total = 0;			// total balance
	double min = 0;				// minimum amount due
	bool loop = true;			// run loop

	while(loop==true)
	{
		cout << "Please enter your account balance. Enter Q to quit the program.\n\n$";
		cin >> input;

		// Exit Program

		if(input=="q" || input=="Q")
			loop = false;

		// Main Code
		else
		{
			// Convert to INT
			balance = atoi(input.c_str());

			// Balance is 1000 or less

			if (balance <= 1000)	
			{
				interest = balance * rate;
				total = balance + interest;
			
				// Find minimum payment due

				if(total <= 10)
					min = total;

				else if (total > 10 && (total/10) > 10)
					min = total / 10;

				else
					min = 10;

				// Output

				cout << fixed;
				cout << "Your balance is $" << balance << ". \n"
				<< "Your interest rate is 1.5% of $" << balance
				<< " which is $" << setprecision(2) << interest << ". \n"
				<< "Your total amount due (balance + interest) is $" << setprecision(2) << total << ". \n"
				<< "Your minimum payment is $" << setprecision(2) << min << ". \n \n";
			}

			// Balance is over 1000

			else if (balance > 1000)
			{
				interest = thousand * rate;
				interest_alt = ( (balance - thousand) * rate_alt );
				total = balance + interest + interest_alt;

				// Find minimum payment due

				if(total <= 10)
					min = total;

				else if (total > 10 && (total/10) > 10)
					min = total / 10;

				else
					min = 10;

				// Output

				cout << fixed;
				cout << "Your balance is $" << setprecision(2) << balance << ". \n"
				<< "Your interest rate is 1.5% of the first $1000.00 of $" << setprecision(2) << balance
				<< "\nwhich is $" << setprecision(2) << interest << " and 1.0% on the remaining $"
				<< setprecision(2) << balance - thousand << "\nwhich is $" << setprecision(2) << interest_alt << ". \n"
				<< "Your total amount due (balance + interest) is $" << setprecision(2) << total << ". \n"
				<< "Your minimum payment is $" << setprecision(2) << min << ". \n \n";
			}
		}
	}
}