#include <iostream>

int max(int num1, int num2)
{
	int max;

	if(num1 > num2)
		max = num1;
	else if(num2 > num1)
		max = num2;

	return max;
}

int main()
{
	int num1;
	int num2;
	std::cout << "First number: ";
	std::cin >> num1;
	std::cout << "Second number: ";
	std::cin >> num2;
	std::cout << max(num1, num2);
	std::cin.ignore();
	std::cin.get();
}