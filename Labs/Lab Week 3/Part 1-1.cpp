#include <iostream>

using namespace std;

int main()
{

int singles = 0;
int doubles = 0;
int triples = 0;
int home_runs = 0;
int at_bats = 0;
double slug_per = 0;

cout << "Let's calculate the hitter's slugging percentage!" << endl;
cout << "Enter the number of singles: ";
cin >> singles;
cout << "Enter the number of doubles: ";
cin >> doubles;
cout << "Enter the number of triples: ";
cin >> triples;
cout << "Enter the number of home runs: ";
cin >> home_runs;
cout << "Enter the number of times at bat: ";
cin >> at_bats;

slug_per = ((singles) + (2 * doubles) + (3 * triples) + (4 * home_runs)) / static_cast <double> (at_bats);

cout << "The hitter's slugging percentage is " << slug_per << "!" << endl;

system("pause");

}