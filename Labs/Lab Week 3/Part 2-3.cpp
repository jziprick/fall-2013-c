#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

// Variables
	int x = 0;
	while (x < 10)
	{
		cout << x << endl;
		x++;  // This is needed to end the loop
	}
	
	cout << endl << endl;

// End of Program

system("pause");

}