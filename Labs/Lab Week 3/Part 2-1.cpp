#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

// Variables

	float owed = 0;
	float payment = 0;
	float balance = 0;
	float orig_balance = 0;
	int   dollars = 0;
	int   quarters = 0;
	int   dimes = 0;
	int   nickels = 0;
	int   pennies = 0;


// User Input
	
	cout << "Please enter the amount owed. \n";
	cin >> owed;
	cout << "Please enter the amount paid. \n";
	cin >> payment;


// Calculations and Output

	if (owed < payment)
	{
		balance = payment - owed;
		orig_balance = balance;
		dollars = balance / 1;
		balance = balance - dollars;
		quarters = balance / .25;
		balance = balance - (quarters * .25);
		dimes = balance / .1;
		balance = balance - (dimes * .1);
		nickels = balance / .05;
		balance = balance - (nickels * .05);
		pennies = balance / .01;
		cout 
			<< "You owe " << owed << " and you paid " << payment << ".\n"
			<< "You are owed " << orig_balance << " which you will receive as follows: \n \n";
			if (dollars == 1)
			{
				cout << "\t - " << dollars << " dollar \n";
			}
			else
			{
				cout << "\t - " << dollars << " dollars \n";
			}
			if (quarters == 1)
			{
				cout << "\t - " << quarters << " quarter \n";
			}
			else
			{
				cout << "\t - " << quarters << " quarters \n";
			}
			if (dimes == 1)
			{
				cout << "\t - " << dimes << " dime \n";
			}
			else
			{
				cout << "\t - " << dimes << " dimes \n";
			}
			if (nickels == 1)
			{
				cout << "\t - " << nickels << " nickel \n";
			}
			else
			{
				cout << "\t - " << nickels << " nickels \n";
			}
			if (pennies == 1)
			{
				cout << "\t - " << pennies << " penny \n";
			}
			else
			{
				cout << "\t - " << pennies << " pennies \n";
			}
	}

	else if (owed > payment)
	{
		cout << "You didn't give enough money!";
	}

	else if (owed == payment)
	{
		cout << "You gave exact change!";
	}

	else
	{
		cout << "Null value";
	}

cout << endl << endl;


// End of Program

system("pause");

}
