#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

int x = 0;
int y = 0;
int temp = 0;

cout << "Enter X: ";
cin >> x;
cout << "Enter Y: ";
cin >> y;

cout << endl << "X = " << setw(4) << x << " Y = " << setw(4) << y << endl;
cout << "--------------------------------------------------------------------------------" << endl;
temp = y;
y = x;
x = temp;
cout << "X = " << setw(4) << x << " Y = " << setw(4) << y << endl;

system("pause");

}
