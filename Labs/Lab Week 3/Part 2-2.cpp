#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

// Variables
	int score = 0;

// Prompt
	cout << "Please enter a score between 1 and 100: ";

// User Input
	cin >> score;
	cout << endl;

// Output
	if (score >= 90)
	{
		cout << "A";
	}
	else if (score >= 80)
	{
		cout << "B";
	}
	else if (score >= 70)
	{
		cout << "C";
	}
	else if (score >= 60)
	{
		cout << "D";
	}
	else if (score < 60)
	{
		cout << "F";
	}
	else
	{
		cout << "Null value";
	}
	
	cout << endl << endl;

// End of Program

system("pause");

}