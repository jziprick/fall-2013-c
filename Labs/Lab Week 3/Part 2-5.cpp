#include <iostream>
#include <string>

using namespace std;

int main()
{
	string grade = "grade";

	cout << "Enter a grade.\n";
	cin >> grade;

	if (grade == "A+")
	{
		cout << "100+";
	}
	else if (grade == "A")
	{
		cout << "93-100";
	}
	else if (grade == "A-")
	{
		cout << "90-92.9";
	}
	else if (grade == "B+")
	{
		cout << "87-89.9";
	}
	else if (grade == "B")
	{
		cout << "83-86.9";
	}
	else if (grade == "B-")
	{
		cout << "80-82.9";
	}
	else
	{
		cout << "Null value";
	}
	cout << endl << endl;
	system("pause");
}