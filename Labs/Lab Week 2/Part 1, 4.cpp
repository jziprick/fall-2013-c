#include <iostream>

using namespace std;

int main(){
    
    int meters = 0;
    float miles = 0;
    float feet = 0;
    float inches = 0;
    
    cout << "Enter a measurement in meters: ";
    cin >> meters;
    miles = meters / 1609.344;
    feet = meters * 3.281;
    inches = feet * 12;
    
    cout << "You entered " << meters << " meters. This converts as follows:" << endl;
    cout << "Miles: " << miles << endl;
    cout << "Feet: " << feet << endl;
    cout << "Inches:" << inches << endl;
    
}