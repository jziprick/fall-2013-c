#include <iostream>

using namespace std;

int main(){
    
int num = 0;
int ten_thousands = 0;
int thousands = 0;
int hundreds = 0;
int tens = 0;
int ones = 0;

cin >> num;

ten_thousands = num / 10000 % 10;
thousands = num / 1000 % 10;
hundreds = num / 100 % 10;
tens = num / 10 % 10;
ones = num % 10;

cout << ten_thousands << " " << thousands << " " << hundreds << " " << tens << " " << ones << endl;
    
}