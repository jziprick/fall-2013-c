#include <iostream>

using namespace std;

int main()
{
  double x = 10.76;
  double y = 1.50;
  const int mult = 100;
  
  double dif = x - y;
  cout << dif << endl;
  
  int intDif = static_cast<int>(dif * mult);

  cout << intDif << endl;
  cout << dif * mult << endl;

  system("pause");
  return 0;
}

// using the IDE Microsoft Visual Studio 2008...

// dif is 9.26 because the value of this variable is the difference between x and y, all of which are double variables

// intDif is 9.26 because the value is product of dif and mult
// although static_cast is used to change to int, both of these values are in the parenthesis so the static cast applies after the multiplication

// intReal isn't in the coding provided.