#include <iostream>
#include <time.h>
#include <cstdlib>
#include <Windows.h>
#include <string>

// FUNCTION PROTOTYPES
void clear ();
void win (int bet, int& money, int game);
void lose (int bet, int& money, int game);
int slots (int& money, int game);
void rollDice (int& die1, int& die2, int& sum);
void prompt (int die1, int die2, int sum);
void round1 (int die1, int die2, int sum, int bet, int& money, int game, int round);
void round2 (int die1, int die2, int sum, int bet, int& money, int game, int round);
int craps (int& money, int game);
void hit (std::string& card, int& sum);
void hitComp (int& sumComp);
int bj (int& money, int game);


// CLEAR SCREEN
void clear ()
{
	for (int i = 0; i < 300; i++)
	{
		std::cout << std::endl;
	}
}

// WIN
void win (int bet, int& money, int game)
{
	switch (game)
	{
		// SLOTS
		case 1:
			break;

		// CRAPS
		case 2:
			money += bet;
			break;
		
		// BLACKJACK
		case 3:
			break;
	}
}

// LOSE
void lose (int bet, int& money, int game)
{
	money -= bet;
}

// SLOT MACHINE GAME
int slots (int& money, int game)
{

	/*
	Keep track of money.
	Three random number slots.
	Input to place bet.
	Error check bet against remaining money.
	Error check that bet is positive and a number.
	Make error checks functions.
	After input new random numbers generate.
	If number slots are all equal win.
	Else lose.
	*/

	int bet = 0;
	char slot1 = 0;
	char slot2 = 0;
	char slot3 = 0;
	bool running = true;
	srand (time(NULL));

	while(running)
	{
		std::cout << "Bet 0 to quit." << std::endl;
		std::cout << "Money:\t" << money << std::endl;
		std::cout << "Bet:\t";
		
		std::cin >> bet;
		if (std::cin.fail ())
		{
			std::cin.clear ();
			bet = 0;
		}

		std::cout << bet;
		
		if(bet > 0)
		{
			std::cout << std::endl << std::endl;
			slot1 = (rand() % 6) + 1;
			slot2 = (rand() % 6) + 1;
			slot3 = (rand() % 6) + 1;
			std::cout << " | " << slot1 << " | " << slot2 << " | " << slot3 << " | ";
			std::cout << "\n\n\n";

			if(slot1 == slot2 && slot2 == slot3)
			{
				money += (bet * 5);
				std::cout << "You win!";
			}
			else
			{
				money -= bet;
				std::cout << "You lose!";
			}

		}

		else if (bet==0)
		{
			running = false;
			clear ();
		}

		else
			std::cout << "Not a valid bet.";
	}
	return 0;
}

// ROLL DICE
void rollDice (int& die1, int& die2, int& sum)
{
	die1 = (rand () % 6) + 1;
	die2 = (rand () % 6) + 1;
	sum = die1 + die2;
}

// CRAPS PROMPT
void prompt (int die1, int die2, int sum)
{
	std::cout << "Press any key to roll dice.\n";
	std::cin.ignore ();
	std::cin.get ();
	rollDice (die1, die2, sum);
	std::cout << die1 << " and " << die2 << " is " << sum;
}


// ROUND 1
void round1 (int die1, int die2, int sum, int bet, int& money, int game, int round)
{
	/*	
	ROUND 1
	In round one, two dice are rolled. 
	If the sum of the dice is either 7 or 11, you win your bet (known as the line bet). 
	If it�s 2,3 or 12, you lose, otherwise known as craps. 
	If it is any other number, the next round begins.
	*/

	prompt (die1, die2, sum);

	if (sum==7 || sum==11)
	{
		// WIN
		win (bet, money, game);
	}

	else if (sum==2 || sum==3 || sum==12)
	{
		// LOSE
		lose (bet, money, game);
	}

	else
		round = 2;
}


// ROUND 2
void round2 (int die1, int die2, int sum, int bet, int& money, int game, int round)
{
	/*
	ROUND 2
	In round 2, two dice are rolled. 
	If the roll is 7, all bets are lost. 
	Otherwise, the rolls continue until the first number rolled is rolled again, winning the bet, and starting at round 1 again.
	*/

	bool running = true;
	while(running)
	{
		if (sum==7)
		{
			// LOSE
			running = false;
			round = 1;
			lose (bet, money, game);
		}
	}
}


// CRAPS GAME
int craps (int& money, int game)
{
	srand (time (0));
	int die1 = 0;
	int die2 = 0;
	int sum = 0;
	int round = 0;
	int bet = 0;
	bool running = true;

	while(running)
	{
	std::cout << "Place bet.\n";
	std::cin >> bet;
	round = 1;

	switch (round)
	{
		// ROUND 1
		case 1:
			round1(die1, die2, sum, bet, money, game, round);

			break;

		// ROUND 2
		case 2:
			round2(die1, die2, sum, bet, money, game, round);

			break;
	}

	}

	return 0;
}

// ROULETTE GAME
int roulette (int&money, int game)
{
	
	return 0;
}
// HIT (PLAYER)
void hit (std::string& card, int& sum)
{
	int value = 0;
	int cardGen = (rand () % 13) + 1;
	switch (cardGen)
	{
		case 1:
			card = 'A';
			if (sum < 22)
				value = 11;
			else
				value = 1;
			break;
	
		case 2:
			card = '2';
			value = 2;
			break;

		case 3:
			card = '3';
			value = 3;
			break;

		case 4:
			card = '4';
			value = 4;
			break;

		case 5:
			card = '5';
			value = 5;
			break;

		case 6:
			card = '6';
			value = 6;
			break;

		case 7:
			card = '7';
			value = 7;
			break;

		case 8:
			card = '8';
			value = 8;
			break;

		case 9:
			card = '9';
			value = 9;
			break;

		case 10:
			card = '10';
			value = 10;
			break;

		case 11:
			card = 'J';
			value = 10;
			break;

		case 12:
			card = 'Q';
			value = 10;
			break;

		case 13:
			card = 'K';
			value = 10;
			break;
	}
	
	sum += value;
}

// HIT (COMPUTER)
void hitComp (int& sumComp)
{
	int value = 0;
	int cardGen = (rand () % 13) + 1;
	switch (cardGen)
	{
		case 1:
			if (sumComp < 22)
				value = 11;
			else
				value = 1;
			break;
	
		case 2:
			value = 2;
			break;

		case 3:
			value = 3;
			break;

		case 4:
			value = 4;
			break;

		case 5:
			value = 5;
			break;

		case 6:
			value = 6;
			break;

		case 7:
			value = 7;
			break;

		case 8:
			value = 8;
			break;

		case 9:
			value = 9;
			break;

		case 10:
			value = 10;
			break;

		case 11:
			value = 10;
			break;

		case 12:
			value = 10;
			break;

		case 13:
			value = 10;
			break;
	}
	
	sumComp += value;
}
// BLACKJACK GAME
int bj (int& money, int game)
{
	int bet = 0;
	std::string card;
	int sum = 0;
	int sumComp = 0;
	bool running = true;

	std::string legend = "Legend:\n\tK : King\n\tQ : Queen\n\tJ : Jack\n\tA : Ace\n\n\n";

	srand (time (0));
	
	// MAIN GAME LOOP
	while (running)
	{
		std::cout << legend;
		std::cout << "Enter the number 0 at any time to quit.\n";
		std::cout << "Place bet.\n";
		std::cin >> bet;
		clear ();
	
		// INITIAL CARDS
		hit (card, sum);
		std::cout << "You drew a " << card;
		hit (card, sum);
		std::cout << " and a " << card << ".\n\n";
		std::cout << "Sum: " << sum << "\n\n";

		// COMPUTER INITIAL CARDS
		hitComp (sumComp);
		hitComp (sumComp);
		std::cout << "Computer drew a ??? and a ???.";
		std::cout << "Computer Sum: " << "???";

		// CHECK FOR BLACKJACK (INSTANT WIN)
		if (sum==21)
		{
			std::cout << "You got a blackjack (21)! You win!";
			win (bet, money, game);
		}
		else if (sumComp==21)
		{
			std::cout << "Computer got a blackjack (21)! You lose!";
			lose (bet, money, game);
		}
		else if (sum==21 && sumComp==21)
		{
			std::cout << "You and Computer BOTH got a blackjack (21)! You tie!";
		}
		else
		{
			bool running2 = true;
			char move;
			while(running2)
			{
				clear ();
				std::cout << "Sum: " << sum << 
				std::cout << "Hit (H) or Stay (S)?";
				std::cin >> move;
				if (move=='H' || move=='h')
				{
					hit (card, sum);
					std::cout << "You drew a " << card << ".\n";
					std::cout << "Sum: " << sum;
					std::cin.ignore ();
					std::cin.get ();
				}
				else if (move=='S' || move=='s')
				{
					std::cout << "You hold at " << sum << ".";
				}
			}
		}
	}


	std::cin.ignore ();
	std::cin.get ();
	
	return 0;
}

// MAIN MENU
int main ()
{
	int money = 100;
	bool running = true;
	int game = 0;

	clear ();

	while (running)
	{
		
		std::cout << "Money: " << money << "\n \n \n"
		<< "Choose a game to play (type corresponding number): \n \n"
		<< "(1) \t Slot Machine\n"
		<< "(2) \t Craps\n"
		<< "(3) \t Roulette\n"
		<< "(4) \t Blackjack\n\n";

		std::cin >> game;
		clear ();

		switch (game)
		{
			case 1:
				slots (money, game);
				break;
			case 2:
				craps (money, game);
				break;
			case 3:
				roulette (money, game);
				break;
			case 4:
				bj (money, game);
				break;
			default:
				std::cout << "Select a valid option.\n\nPress any key to continue.\n\n";
				std::cin.ignore ();
				std::cin.get ();
				break;
		}
	}
}